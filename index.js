export class PomodoroTimer {
  constructor() {
    this.increment = 5;
    this.countdown = 0;
    this.seconds = 1500;
    this.workTime = 25;
    this.breakTime = 5;
    this.isBreak = true;
    this.isPaused = true;
    this.setButtonSelectors();
    this.bindEvents();
    this.updateInterval = this.setUpdateInterval();
  }

  setUpdateInterval() {
    return window.setInterval(this.updateHTML.bind(this), 100);
  }

  setButtonSelectors() {
    this.status = this.setButton('#status');
    this.timerDisplay = this.setButton('.timerDisplay');
    this.startBtn = this.setButton('#start-btn');
    this.resetBtn = this.setButton('#reset');
    this.workMin = this.setButton('#work-min');
    this.breakMin = this.setButton('#break-min');
    this.breakPlusBtn = this.setButton('#break-plus');
    this.breakMinusBtn = this.setButton('#break-minus');
    this.workPlusBtn = this.setButton('#work-plus');
    this.workMinusBtn = this.setButton('#work-minus');
  }

  setButton(selector) {
    return document.querySelector(selector);
  }

  timer() {
    this.seconds--;
    if (this.seconds < 0) {
      clearInterval(this.countdown);
      this.seconds = (this.isBreak ? this.breakTime : this.workTime) * 60;
      this.isBreak = !this.isBreak;
      this.countdown = setInterval(this.timer, 1000);
    }
  }

  countdownDisplay() {
    let minutes = Math.floor(this.seconds / 60);
    let remainderSeconds = this.seconds % 60;
    this.timerDisplay.textContent = `${minutes}:${
      remainderSeconds < 10 ? '0' : ''
    }${remainderSeconds}`;
  }

  updateHTML() {
    this.countdownDisplay();
    this.buttonDisplay();
    this.isBreak
      ? (this.status.textContent = 'Radi!')
      : (this.status.textContent = 'Uzmi pauzu!');
    this.workMin.textContent = this.workTime;
    this.breakMin.textContent = this.breakTime;
  }

  bindEvents() {
    document.onclick = this.updateHTML.bind(this);
    this.startBtn.addEventListener('click', this.startClick.bind(this));
    this.resetBtn.addEventListener('click', this.resetClick.bind(this));
    this.workMinusBtn.addEventListener('click', this.workMinusClick.bind(this));
    this.workPlusBtn.addEventListener('click', this.workPlusClick.bind(this));
    this.breakMinusBtn.addEventListener(
      'click',
      this.breakMinusClick.bind(this)
    );
    this.breakPlusBtn.addEventListener('click', this.breakPlusClick.bind(this));
  }

  resetClick() {
    clearInterval(this.countdown);
    this.seconds = this.workTime * 60;
    this.countdown = 0;
    this.isPaused = true;
    this.isBreak = true;
  }

  startClick() {
    clearInterval(this.countdown);
    this.isPaused = !this.isPaused;
    if (!this.isPaused) {
      this.countdown = setInterval(this.timer.bind(this), 1000);
    }
  }

  workPlusClick() {
    this.workTime = this.increaseTimer(this.workTime + this.increment);
  }

  workMinusClick() {
    this.workTime = this.decreaseTimer(this.workTime - this.increment);
  }

  breakPlusClick() {
    this.breakTime = this.increaseTimer(this.breakTime + this.increment);
  }

  breakMinusClick() {
    this.breakTime = this.decreaseTimer(this.breakTime - this.increment);
  }

  decreaseTimer(time) {
    return Math.max(time, 5);
  }

  increaseTimer(time) {
    return Math.min(time, 60);
  }

  buttonDisplay() {
    if (this.isPaused && this.countdown === 0) {
      this.startBtn.textContent = 'Počni';
    } else if (this.isPaused && this.countdown !== 0) {
      this.startBtn.textContent = 'Nastavi';
    } else {
      this.startBtn.textContent = 'Pauza';
    }
  }
}

new PomodoroTimer();
