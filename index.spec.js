import { PomodoroTimer } from './index.js';

describe('PomodoroTimer', () => {
  it('should bind timer button events', () => {
    let timer = new PomodoroTimer();
    timer.setButtonSelectors();
    expect(timer.status).not.toBeNull();
  });
  it('should bind timer display events', () => {
    let timer = new PomodoroTimer();
    timer.setButtonSelectors();
    expect(timer.timerDisplay).not.toBeNull();
  });
  it('should bind start button events', () => {
    let timer = new PomodoroTimer();
    timer.setButtonSelectors();
    expect(timer.startBtn).not.toBeNull();
  });
  it('should bind reset button events', () => {
    let timer = new PomodoroTimer();
    timer.setButtonSelectors();
    expect(timer.resetBtn).not.toBeNull();
  });
  it('should bind work minus button events', () => {
    let timer = new PomodoroTimer();
    timer.setButtonSelectors();
    expect(timer.workMin).not.toBeNull();
  });
  it('should bind break minus events', () => {
    let timer = new PomodoroTimer();
    timer.setButtonSelectors();
    expect(timer.breakMin).not.toBeNull();
  });
  it('should bind break plus events', () => {
    let timer = new PomodoroTimer();
    timer.setButtonSelectors();
    expect(timer.breakPlusBtn).not.toBeNull();
  });
  it('should bind break minus events', () => {
    let timer = new PomodoroTimer();
    timer.setButtonSelectors();
    expect(timer.breakMinusBtn).not.toBeNull();
  });
  it('should bind work plus events', () => {
    let timer = new PomodoroTimer();
    timer.setButtonSelectors();
    expect(timer.workPlusBtn).not.toBeNull();
  });
  it('should bind work minus events', () => {
    let timer = new PomodoroTimer();
    timer.setButtonSelectors();
    expect(timer.workMinusBtn).not.toBeNull();
  });

  it('should return start button html element', () => {
    let timer = new PomodoroTimer();
    let button = timer.setButton('#start-btn');
    let buttonDoc = document.getElementById('start-btn');
    expect(button).toEqual(buttonDoc);
  });
  it('should return reset button html element', () => {
    let timer = new PomodoroTimer();
    let button = timer.setButton('#reset');
    let buttonDoc = document.getElementById('reset');
    expect(button).toEqual(buttonDoc);
  });
  it('should return break plus button html element', () => {
    let timer = new PomodoroTimer();
    let button = timer.setButton('#break-plus');
    let buttonDoc = document.getElementById('break-plus');
    expect(button).toEqual(buttonDoc);
  });
  it('should return break-minus button html element', () => {
    let timer = new PomodoroTimer();
    let button = timer.setButton('#break-minus');
    let buttonDoc = document.getElementById('break-minus');
    expect(button).toEqual(buttonDoc);
  });
  it('should return work plus button html element', () => {
    let timer = new PomodoroTimer();
    let button = timer.setButton('#work-plus');
    let buttonDoc = document.getElementById('work-plus');
    expect(button).toEqual(buttonDoc);
  });
  it('should return work minus button html element', () => {
    let timer = new PomodoroTimer();
    let button = timer.setButton('#work-minus');
    let buttonDoc = document.getElementById('work-minus');
    expect(button).toEqual(buttonDoc);
  });

  it('should decrease work timer', () => {
    let timer = new PomodoroTimer();
    let workTime1 = timer.workTime;
    timer.workMinusClick();
    let workTime2 = timer.workTime;
    expect(workTime2).toEqual(workTime1 - timer.increment);
  });
  it('should increase work timer', () => {
    let timer = new PomodoroTimer();
    let workTime1 = timer.workTime;
    timer.workPlusClick();
    let workTime2 = timer.workTime;
    expect(workTime2).toEqual(workTime1 + timer.increment);
  });
  it('should decrease break timer', () => {
    let timer = new PomodoroTimer();
    let breakTime1 = timer.breakTime;
    timer.breakMinusClick();
    let breakTime2 = timer.breakTime;
    expect(breakTime1).toEqual(breakTime2);
  });
  it('should increase break timer', () => {
    let timer = new PomodoroTimer();
    let breakTime1 = timer.breakTime;
    timer.breakPlusClick();
    let breakTime2 = timer.breakTime;
    expect(breakTime2).toEqual(breakTime1 + timer.increment);
  });

  it('should increase timer', () => {
    let timer = new PomodoroTimer();
    expect(timer.increaseTimer(20)).toEqual(20);
    expect(timer.increaseTimer(70)).toEqual(60);
  });
  it('should decrease timer', () => {
    let timer = new PomodoroTimer();
    expect(timer.decreaseTimer(20)).toEqual(20);
    expect(timer.decreaseTimer(0)).toEqual(5);
  });
});
