# Pomodoro Timer, Marko Markanović

Testovi za Pomodoro Timer, testovi su izvršeni uz pomoć https://jasmine.github.io.

  - should bind timer button events (testira da je  element koji prikazuje status pauza ili rad ucitan)
  - should bind timer display events (testira da je element koji prikazuje timer ucitan)
  - should bind start button events (testira da je button za start ucitan, te da su povezani click eventovi)
  - should bind reset button events (testira da je button za reset ucitan, te da su povezani click eventovi)
  - should bind work plus events (testira da je button za povecanja trajanja rada ucitan te da su click eventi povezani)
  - should bind work minus button events (testira da je button za povecanja trajanja rada ucitan te da su click eventi povezani)
  - should bind break minus events (testira da je button za smanjenje trajanja rada ucitan te da su click eventi povezani)
  - should bind break plus events (testira da je button za povecanje trajanja pauze ucitan te da su click eventi povezani)
  - should return start button html element (testira dostupnost start buttona)
  - should return reset button html element (testira dostupnost reset buttona)
  - should return break plus button html element (testira dostupnost odmor plus buttona)
  - should return break-minus button html element (testira dostupnost odmor minus buttona)
  - should return work plus button html element (testira dostupnost rad plus buttona)
  - should return work minus button html element (testira dostupnost rad minus buttona)
  - should decrease work timer (testira smanjenje timera za rad)
  - should increase work timer (testira povecanje timera za rad)
  - should decrease break timer (testira smanjenje timera za pauzu)
  - should increase break timer (testira smanjenje timera za rad)
  - should increase timer (testira smanjenje timera te osigarava da su timeri unutar limita)
  - should decrease timer (testira smanjenje timera te osigurava da su timeri unutar limita)